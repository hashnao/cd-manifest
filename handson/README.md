# Starter Kit : 運用編3日目 CD ハンズオン

Container Stater Kit で実施するハンズオンの手順を紹介します。  
当ハンズオンによって、OpenShift GitOps を使ったアプリケーションの CD(継続的デリバリー) を体験することができます。

## 【ハンズオン受講者向け】
## [1 章: CD でデプロイするアプリケーション環境の事前準備](./text/Chapter-1.md)
## [2 章: OpenShift GitOps を使ったアプリケーションのデプロイ](./text/Chapter-2.md)
## [3 章: 開発環境のアプリケーション更新をステージング環境～本番環境に反映](./text/Chapter-3.md)