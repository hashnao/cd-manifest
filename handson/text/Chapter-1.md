# 1 章: CD でデプロイするアプリケーション環境の事前準備

Container Stater Kit で実施するハンズオンの手順を紹介します。  
当ハンズオンによって、OpenShift GitOps を使ったアプリケーションの CD(継続的デリバリー) を体験することができます。

本章では、アプリケーションの実行環境の事前準備に取り掛かります。  

当ハンズオンでは、アプリケーション本体と、そのアプリケーションを展開するためのマニフェストの2種類のコードを使用します。  
これらのコードは、当ハンズオン参加者の Gitea にリポジトリがクローンされています。


---
## 1-1. ユーザ情報を環境変数に登録
OpenShift クラスタに対し oc コマンドが実行可能な環境において、ユーザ情報や Gitea のホスト名を環境変数として登録します。
これら環境変数は、当ハンズオンのコマンド中や、ファイル中の文字列との置き換えなどで使用されるため、必要な手順です。

```
## ユーザ名
export USER=$(oc whoami)

## Gitea のホスト名
export GITEA_HOSTNAME=$(oc get route gitea -n ${USER}-develop -ojsonpath='{.spec.host}')
```

次に、Gitea のアクセストークンを作成します。
アクセストークンは、当ハンズオンで OpenShift Pipelines や OpenShift GitOps が、あなたの Gitea リポジトリを操作するために必要となります。

以下のコマンドより Gitea のURLを取得し、ブラウザから gitea/openshift で Gitea にログインします。
```
echo http://${GITEA_HOSTNAME}
```

Gitea 画面右上の "Profile and Settings..." アイコンから "Settings" を選択します。

![Gitea Generate Token 01](../img/gitea-generate-token-01.png)

"Application" タブを開き、"Token Name" に任意の名前 (e.g. gitea-token) を入力し、**"Generate Token"** ボタンを押します。

![Gitea Generate Token 02](../img/gitea-generate-token-02.png)

自動的にアクセストークンが作成され、画面上部に表示されます。  
**表示されているアクセストークンは、このページから移動すると二度と表示できなくなるため、どこかに記録しておきましょう。**  
※もしアクセストークンを失ってしまったら、もう一度同じ手順で作り直して下さい。

作成したアクセストークンを環境変数に登録します。
```
## Gitea のtoken
export GITEA_TOKEN=<作成したトークン>
```


---
## 1-2. CI パイプラインの作成と実行

当ハンズオンでは以下の3つの Project が事前に作成されています。

* {USER}-develop
* {USER}-staging
* {USER}-production

名前から感づかれる方もいると思いますが、それぞれの Project は、開発環境、ステージング環境、本番環境の3つの環境を模しています。

また、アプリケーションのコンテナイメージをビルドする、CI パイプラインを作成し、実行します。  
当ハンズオンでは後段で、cd-app リポジトリのソースコードを更新します。このパイプラインは、更新されたアプリケーションコードから新しいコンテナイメージを簡単にビルドするために使います。

次のコマンドを全てコピーしてターミナルにペーストして実行すると、パイプラインが作成/実行されます。

```
# Clone repositories
git clone http://${GITEA_HOSTNAME}/gitea/cd-manifest.git
git clone http://${GITEA_HOSTNAME}/gitea/cd-app.git

# Replace <~> string to your own variables
cd ./cd-manifest/
find . -type f -name "*.yaml" -print0 | xargs -0 sed -i "s/<USER>/${USER}/g"
find . -type f -name "*.yaml" -print0 | xargs -0 sed -i "s/<GITEA_HOSTNAME>/${GITEA_HOSTNAME}/g"
find . -type f -name "*.yaml" -print0 | xargs -0 sed -i "s/<GITEA_TOKEN>/${GITEA_TOKEN}/g"

# Push to remote cd-manifest repository
git config --global user.name gitea
git config --global user.email admin@gitea.com
git commit -a -m "Replaced GITEA_ info"
git push

# Create CI pipeline
oc project ${USER}-develop
oc create -f ./handson/pipelines/prep/gitlab-auth.yaml
oc secret link pipeline gitlab-token
oc create -f ./handson/pipelines/prep/tekton-pvc.yaml
oc create -f ./handson/pipelines/tasks/
oc create -f ./handson/pipelines/handson-pipeline.yaml

# Run CI pipeline
oc create -f ./handson/pipelines/handson-pipelinerun.yaml
```


---
## 1-3. パイプライン実行の確認
OpenShift Web コンソールに入って、パイプラインがうまく実行されたか確認してみましょう。

Web コンソールの左のメニューから **"Pipelines" > "Pipelines"** を選択し、画面上部の Project を選択する場所で **"{USER}-develop"** を選択します。  
そして、右側の画面で **"PipelineRuns"** のタブをクリックすると、"PLR" というマークが付いたエントリーが見つかります。  
これの Status が **"Succeeded"** になっていれば成功です。何か処理が動いているようであれば、終わるまで待ちます。(おおむね5分も待てばよいでしょう)

![PipelineRun Success 01](../img/pipelinerun-success-01.PNG)

エントリーの名前をクリックすると、実行されたパイプラインの中身やログを見ることができます。

![PipelineRun Success 02](../img/pipelinerun-success-02.PNG)

---
事前準備作業は以上です。