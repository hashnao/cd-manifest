# 2. OpenShift GitOps を使ったアプリケーションのデプロイ

それでは、OpenShift GitOps を使って、

* 開発環境 : {USER}-develop
* ステージング環境 : {USER}-staging
* 本番環境 : {USER}-production

のそれぞれの環境に、アプリケーションをデプロイしていきます。

---
## 2-1. Argo CD UI へのログイン
OpenShift GitOps は内部で Argo CD というオープンソースのソフトウェアが稼働しています。  
Argo CD は GUI を持っており、OpenShift の OAuth でログインすることができます。ここでログインしてみましょう。

OpenShift Web コンソールにログインすると、画面上部に小さな四角が3x3で並んだアイコンが見つかります。  
ここをクリックすると、**"Cluster Argo CD"** というエントリーが見つかります。これをクリックします。

![Argo CD UI Login 01](../img/argocd-login-01.PNG)

すると、別ウィンドウで Argo CD の UI が開きます。

![Argo CD UI Login 02](../img/argocd-login-02.PNG)

この画面の右側にある、**"LOG IN VIA OPENSHIFT"** をクリックすると、OpenShift Web コンソールのログインの画面に移ります。  
ログインすると、次のようなアクセス認証の画面が表示される場合があるので、**user:Info** にチェックを入れたまま **"Allow selected permissions"** ボタンをクリックします。

![Argo CD UI Login 03](../img/argocd-login-03.PNG)

Argo CD の GUI にログインします。現時点では OpenShift GitOps で何も設定していないので、"No applications yet" と何もない状態で表示されています。

![Argo CD UI Login 04](../img/argocd-login-04.PNG)

---
## 2-2. マニフェストリポジトリの接続

ログインしたら初めに、Argo CD に Gitea 上のあなたのマニフェストリポジトリを接続します。  
これは Argo CD がマニフェストを取得するために必要な作業です。

UI の左端にある歯車のアイコンをクリックすると、"Settings" 画面に移ります。そこで一番上にある **"Repositories"** をクリックします。

![Argo CD Repo 01](../img/argocd-repo-01.PNG)

下図のような画面になります。ここで **"CONNECT REPO USING HTTPS"** をクリックします。

![Argo CD Repo 02](../img/argocd-repo-02.PNG)

すると接続するリポジトリの情報を入力する画面が表示されます。以下の項目を入力して、**"CONNECT"** ボタンをクリックします。

* Type  
git
* Project  
(空白)
* Repository URL  
あなたの Gitea の cd-manifest リポジトリの URL ※末尾に ".git" を忘れないように注意
* Username
gitea
* Password 
openshift

![Argo CD Repo 03](../img/argocd-repo-03.PNG)

"CONNECTION STATUS" が "Successful" と表示されれば、接続に成功です。

![Argo CD Repo 04](../img/argocd-repo-04.PNG)

---
## 2-3. 開発環境でのアプリケーションのデプロイ

それでは、OpenShift GitOps を使って開発環境にアプリケーションをデプロイします。  

OpenShift GitOps には2つの重要なリソースがあります。  
1つ目は **"Application"** です。 
"Application" には、そのアプリケーションをデプロイするためのマニフェストのリンクや、デプロイ先の Project などが記録されています。  
2つ目は **"AppProject"** です。
"AppProject" は OpenShift とはまた別の "Project" の概念で、1つ以上の "Application" を稼働させるためのグループです。"Application" は何らかの "AppProject" に紐づく必要があります。

よって、これら2つのリソースを作成することが必要です。

当ハンズオンでは3つの環境ごとに、

* "{USER}-app-develop" Application <--> "{USER}-dev" AppProject 
* "{USER}-app-staging" Application <--> "{USER}-stg" AppProject 
* "{USER}-app-production" Application <--> "{USER}-prod" AppProject 

という、Application と AppProject のリソースの関係を持つことにします。

それでは開発環境の Application と AppProject を作成します。これらのマニフェストはすでに作られているので、以下のコマンドを実行するだけでよいです。

```
oc policy add-role-to-user admin system:serviceaccount:openshift-gitops:openshift-gitops-argocd-application-controller -n ${USER}-develop

  [出力例]
  clusterrole.rbac.authorization.k8s.io/admin added: "system:serviceaccount:openshift-gitops:openshift-gitops-argocd-application-controller"

cd ./cd-manifest/handson/argocd

oc create -f dev-project.yaml

  [出力例]
  appproject.argoproj.io/{USER}-dev created

oc create -f dev-app.yaml

  [出力例]
  application.argoproj.io/{USER}-app-develop created
```

コマンドを実行したら、Argo CD の画面を見に行きましょう。  
先ほどは何も表示されていなかった画面に、"{USER}-app-develop" Application が表示されています。うまく行っていれば、Appliation の左縁は緑色になっていて、Status は "Healthy" と表示されているはずです。  
また、Project には "{USER}-dev" と AppProject の名前が表示されていることも確認しましょう。

![Argo CD Dev App 01](../img/argocd-dev-app-01.PNG)

Application の箱をクリックすると、この Application によってどのような OpenShift リソースがデプロイされているかなどの詳細が表示されます。

![Argo CD Dev App 02](../img/argocd-dev-app-02.PNG)

実際に OpenShift クラスタ側でもこれらのリソースを確認することができます。  
例えば、Route リソースを見に行ってみましょう。

OpenShift Web コンソールにログインし、左のメニューから **"Networking" > "Routes"** を選択し、画面上部の Project を選択する場所で **"{USER}-develop"** を選択します。  
すると、右側の画面で "health-record" というエントリーが見つかるでしょう。これが OpenShift GitOps によって作られた Route リソースです。  

![Argo CD Dev App 03](../img/argocd-dev-app-03.PNG)

"health-record" リソースの "Location" の列にある "http://" で始まる URL をクリックすると、別ウィンドウで "Demo Health" と表示された Web アプリケーションの画面が開きます。これが cd-app リポジトリに格納されていたアプリケーションです。

このアプリケーションはその名の通りデモアプリケーションなので、ユーザ認証の機能は実装していません。"Login" の欄で、何でもよいので適当な名前を入力して "Sign In" ボタンを押すと、次のような画面が表示されます。  

![Argo CD Dev App 04](../img/argocd-dev-app-04.PNG)

以上で、OpenShift GitOps を使って開発環境にアプリケーションをデプロイすることができました。

---
## 2-4. 同期の確認
もう少し深く Argo CD について探索していきましょう。

先ほどの Argo CD の GUI で、Application に **"Synced"** というステータス表示がありました。覚えていない方は、もう一度確認してみて下さい。 

![Argo CD Dev App 02](../img/argocd-dev-app-02.PNG)

これは、マニフェストリポジトリで宣言しているリソースと、OpenShift クラスタでデプロイされているリソースの実態が、"Sync"、つまり同期していることを意味します。

ここで、意図的にどちらかに変更を加えて、この同期をズラしてみるとどうなるでしょうか。

### 2-4-1. 実態に変更を加えた場合

OpenShift クラスタの実態の方に変更を加えてみます。例えば、現在アプリケーションの Pod は1つだけデプロイされています (上図の一番右側の列) が、これを意図的に増やしてみます。

OpenShift Web コンソールにログインし、左のメニューから **"Workloads" > "Deployments"** を選択し、画面上部の Project を選択する場所で **"{USER}-develop"** を選択します。  
すると、右側の画面で "health-record" というエントリーがあるので、このエントリーの右側にある、縦に点が3つ並んだアイコンをクリックします。いくつかメニューが表示されるので、一番上にある **"Edit Pod count"** を選択します。

![Argo CD Sync 01](../img/argocd-sync-01.PNG)

すると、"Edit Pod count" というポップアップが出てきます。現在は Pod が1つなので1が表示されています。これを適当な数に増やして、**"Save"** ボタンをクリックします。

![Argo CD Sync 02](../img/argocd-sync-02.PNG)

上図では、Pod count を5に設定しているので、OpenShift の機能で Pod が5つにスケールアウトされるはずです。

さて、Pod の数は増えたでしょうか ?

おそらく、増えていないでしょう。これは何度やっても増えません。反対に、Pod count を0に減らしても、すぐに1に戻ります。  
なぜ Pod の数が変わらないかと言うと、マニフェストで Pod の数を1と宣言しているためです。

OpenShift GitOps では Argo CD が常にアプリケーションを監視していて、マニフェストで宣言されている内容と実態にズレが出ると (このズレを "ドリフト" と呼びます)、マニフェスト通りになるよう自動的に修正して同期します。  
したがって、Pod の数だけではなく Route や Service など、マニフェストで宣言されているリソースは全て、ドリフトが発生すると修正されます。

この自動的に同期するポリシーを **"Auto-sync"** と呼びます。  
Auto-sync の様子は、OpenShift Web コンソールと Argo CD の画面を横に並べて、先ほどのように Pod count を増やしてみるとわかりやすいです。  
Pod count を変更すると、直ちに Argo CD の Application は **"OutOfSync"** のステータスに変化し、そして Auto-sync によって修正される様子が見られるでしょう。

![Argo CD Sync 03](../img/argocd-sync-03.PNG)

### 2-4-2. マニフェストに変更を加えた場合

今度は反対に、マニフェストに変更を加えるとどうなるか見てみましょう。先ほどと同様に、Pod の数を変化させてみます。

Gitea の cd-manifest リポジトリのソースコードの変更は、クローンしたローカルの cd-manifest リポジトリを編集して `git push` する方法でもいいですし、Gitea の画面から直接変更する方法でも構いません。ここでは Gitea の画面から直接変更する方法でやってみます。

まず、Gitea の cd-manifest リポジトリの画面に行きます。**master ブランチ** が選択された状態で、画面にディレクトリ/ファイル一覧が表示されています。

当ハンズオンで Argo CD が参照しているマニフェストは、`./handson/deploy/` 以下にあります。Pod の数を宣言しているマニフェストファイルは `./handson/deploy/base/health-deploy.yaml` です。

このディレクトリ/ファイルの一覧で、**"handson" > "deploy" > "base" > "health-deploy.yaml"** と順番に辿って、"health-deploy.yaml" ファイルの画面に移ります。  

![Gitea CD Manifest modify 01](../img/gitea-cd-manifest-modify-01.png)

画面には HTML のコードが表示されていますが、コードの右上に表示された **"Edit File"** ボタンをクリックします。
すると、ファイルを編集できるエディタの画面に移ります。  
`health-deploy.yaml` ファイルの 8 行目にある、`replicas: 1` という部分が Pod の数を指定している部分です。  
この部分を、`replicas: 2` と変更して、Pod を 2 つに増やしてみましょう。

変更したら、**"Commit directly to the master branch."** のラジオボタンをチェックして、**"Commit Changes"** ボタンを押します。コミットメッセージを入力しても構いませんし、しなくても構いません。  

![Gitea CD Manifest modify 02](../img/gitea-cd-manifest-modify-02.png)

コミットしたら改めて "health-deploy.yaml" ファイルの中身を見て、変更が反映されていることを確認して下さい。

それでは Argo CD の UI を開いて待ちます。5分も待てば充分です。   
しばらくすると、新しい Pod が追加されて2つになります。OpenShift クラスタの方でも、Pod が2つになっていることが確認できるでしょう。

![Argo CD Sync 04](../img/argocd-sync-04.PNG)

このように、マニフェストに変更を加えると、クラスタの実態はマニフェストに合うように同期されます。

つまり、OpenShift クラスタを一切触ることなく、

**「マニフェストのみを変更することで、実態のアプリケーションに変更を加えていくことができる」** 

これこそが、GitOps の本質的な部分です。

---
## 2-5. ステージング環境でのアプリケーションのデプロイ

それでは、ステージング環境へアプリケーションをデプロイしてみましょう。

開発環境と同じように、次のコマンドを実行します。
```
oc policy add-role-to-user admin system:serviceaccount:openshift-gitops:openshift-gitops-argocd-application-controller -n ${USER}-staging

  [出力例]
  clusterrole.rbac.authorization.k8s.io/admin added: "system:serviceaccount:openshift-gitops:openshift-gitops-argocd-application-controller"

cd ./cd-manifest/handson/argocd

oc create -f stg-project.yaml

  [出力例]
  appproject.argoproj.io/{USER}-stg created

oc create -f stg-app.yaml

  [出力例]
  application.argoproj.io/{USER}-app-staging created
```
開発環境では、このコマンドを実行するだけで自動的にデプロイされましたが、ステージング環境ではどうでしょうか ?  
おそらく、"{USER}-app-staging" Application は作られているものの、リソースはデプロイされていないでしょう。

これは、ステージング環境の "{USER}-app-staging" Application は、cd-manifest リポジトリの **staging ブランチ** を参照するようになっているためです。

今あなたの cd-manifest リポジトリには、**master ブランチ** 1つだけある状態です。  
これは下のように、Gitea の cd-manifest リポジトリの画面で、"Branch: master" と表示されているドロップダウンメニューを開くことで確認できます。

![Argo CD Stg App 01](../img/gitea-argocd-stg-app-01.png)

開発環境では、Application が **master ブランチ** を参照するようになっていたので、リソースはデプロイされました。  

したがって、ステージング環境のために **staging ブランチ** を作成することが必要です。すぐに作りましょう。  
ブランチを作成する方法は Gitea の画面でも、`git` コマンドでもどちらもありますが、ここでは Gitea 画面から作ります。  
Gitea の cd-manifest リポジトリの画面で、"Branchs" タブから "master" ブランチの `Create new branch from 'master'` ボタンを押します。

![Argo CD Stg App 02](../img/gitea-argocd-stg-app-02.png)

"Create new branch" と書かれた画面に移ります。この画面の "Branch name" に **"staging"** と入力します。
入力したら **"Create branch"** ボタンをクリックします。

![Argo CD Stg App 03](../img/gitea-argocd-stg-app-03.png)

cd-manifest リポジトリの画面に戻り、**staging ブランチ** が作られていることが確認できるでしょう。  
下図の画面で開いているメニューから **staging ブランチ** と **master ブランチ** を切り替えることができます。
切り替えてみると、両方のブランチが全く同じファイルを持っていることが確認できると思います。

![Argo CD Stg App 04](../img/gitea-argocd-stg-app-04.png)

さて、ここで再び Argo CD の画面で "{USER}-app-staging" Application を見てみましょう。どうでしょうか ?  
開発環境と全く同じように、リソースがデプロイされているはずです。デプロイされていない場合は、もう少し待ってみて下さい。

Gitea の cd-manifest リポジトリに master ブランチと全く同じファイルを持つ staging ブランチができたことで、Argo CD によって開発環境と全く同じようにステージング環境でもアプリケーションがデプロイされたことがわかります。

![Argo CD Stg App 05](../img/argocd-stg-app-05.PNG)

最後に、開発環境でも行ったように、OpenShift Web コンソールで、"{USER}-staging" Project に作られた "health-record" Route の Location URL にアクセスして、"Demo Health" Web アプリケーションが稼働していることを確認してみて下さい。

---
## 2-6. 【演習】本番環境でのアプリケーションのデプロイ

次は本番環境でのアプリケーションのデプロイを行います。

これは、前節のステージング環境でのアプリケーションのデプロイと、同じ手順で実施できます。  
そのため本節は演習としたいと思います。詳細な手順は記載しませんが、ヒントを残しますので、挑戦してみて下さい。

本番環境でも次のようにアプリケーションがデプロイできたら OK です。

![Argo CD Prod App 01](../img/argocd-prod-app-01.PNG)

### 【ヒント】

* 最初に実行するコマンド
```
oc policy add-role-to-user admin system:serviceaccount:openshift-gitops:openshift-gitops-argocd-application-controller -n ${USER}-production
cd ./cd-manifest/handson/argocd
oc create -f prod-project.yaml
oc create -f prod-app.yaml
```
* Gitea でのブランチ作成  
作成するブランチの名前 : **production**  
元となるブランチ : **staging**

---
OpenShift GitOps を使ったアプリケーションのデプロイは以上です。